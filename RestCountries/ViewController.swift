//
//  ViewController.swift
//  RestCountries
//
//  Created by Giuseppe Bottiglieri on 28/07/17.
//  Copyright © 2017 GiuBot. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, CountryRequestDelegate {
    
    fileprivate let countryRequest = CountryRequest()
    
    internal var countryResult:[CountryModel] = []
    
    var retrieveButton : UIButton = {
        let b = UIButton()
        b.setTitle("Get the list", for: .normal)
        b.backgroundColor = .red
        return b
    }()
    
    var collectionView: UICollectionView = {
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        view.backgroundColor = .yellow
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .yellow
        
        countryRequest.delegate = self
        
        retrieveButton.frame = CGRect(x: 8, y: 80, width: self.view.frame.width-16, height: 50)
        
        collectionView.frame = CGRect(x: 0, y: retrieveButton.frame.maxY,
                                      width: self.view.bounds.width, height: self.view.bounds.height-retrieveButton.frame.maxY)
        collectionView.register(CountryCell.self, forCellWithReuseIdentifier: "CountryCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.view.addSubview(collectionView)
        self.view.addSubview(retrieveButton)
        
        let retrieveGesture = UITapGestureRecognizer(target: self, action: #selector(ViewController.onButtonTapped(recognizer:)))
        retrieveButton.addGestureRecognizer(retrieveGesture)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 16, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCell", for: indexPath) as! CountryCell
        cell.item = countryResult[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return countryResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func onButtonTapped(recognizer: UITapGestureRecognizer) {
        retrieveButton.isEnabled = false
        countryResult.removeAll()
        collectionView.reloadData()
        countryRequest.getList()
    }
    
    func onSuccess(list: [CountryModel]) {
        print("onSuccess")
        countryResult = list
        collectionView.reloadData()
        retrieveButton.isEnabled = true
    }
    
    func onFailure() {
        print("onFailure")
        alert(title: "Errore", message: "Riprova più tardi")
        retrieveButton.isEnabled = true
    }
    
    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

