//
//  CountryModel.swift
//  RestCountries
//
//  Created by Giuseppe Bottiglieri on 28/07/17.
//  Copyright © 2017 GiuBot. All rights reserved.
//

import Foundation

class CountryModel : NSObject {
    var name = ""
    var region = ""
    var population = ""
    
    init(name: String, region: String, population: String) {
        self.name = name
        self.region = region
        self.population = population
    }
}
