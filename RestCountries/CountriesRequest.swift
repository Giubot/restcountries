//
//  CountriesRequest.swift
//  RestCountries
//
//  Created by Giuseppe Bottiglieri on 28/07/17.
//  Copyright © 2017 GiuBot. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol CountryRequestDelegate {
    func onSuccess(list: [CountryModel])
    func onFailure()
}

class CountryRequest {
    
    fileprivate static let url = "https://restcountries.eu/rest/v2/name/united"
    
    var delegate: CountryRequestDelegate?
    
    func getList() {
        Alamofire.request(CountryRequest.url)
            .responseJSON { response in
                
                switch response.result {
                case .success(let value):
                    let jsonObj = JSON(value)
                    
                    print(jsonObj)
                    
                    var list:[CountryModel] = []
                    
                    for data in jsonObj.arrayValue {
                        list.append(CountryModel.init(
                            name: data["name"].stringValue,
                            region: data["region"].stringValue,
                            population: data["population"].stringValue)
                        )
                    }
                    
                    self.delegate?.onSuccess(list: list)
                    
                case .failure(let error):
                    print("error: \(error._code)")
                    
                    self.delegate?.onFailure()
                }
        }
    }
}
