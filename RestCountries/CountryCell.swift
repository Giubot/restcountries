//
//  CountryCell.swift
//  RestCountries
//
//  Created by Giuseppe Bottiglieri on 28/07/17.
//  Copyright © 2017 GiuBot. All rights reserved.
//

import Foundation
import UIKit

class CountryCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var item:CountryModel! {
        didSet {
            self.setup()
        }
    }
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.text = " "
        label.textColor = .black
        return label
    }()
    
    let regionLabel : UILabel = {
        let label = UILabel()
        label.textColor = .red
        return label
    }()
    
    let populationLabel : UILabel = {
        let label = UILabel()
        label.textColor = .blue
        return label
    }()
    
    let dividerView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "divider")
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    func setup() {
        nameLabel.text       = " " + self.item.name
        regionLabel.text     = "   Region: " + self.item.region
        populationLabel.text = "   Population: " + self.item.population
    }
    
    func setupViews() {
        addSubview(nameLabel)
        addSubview(regionLabel)
        addSubview(populationLabel)
        
        nameLabel.sizeToFit()
        
        nameLabel.frame       = CGRect(x: 0, y: 8, width: frame.width, height: nameLabel.frame.height)
        regionLabel.frame     = CGRect(x: 0, y: nameLabel.frame.maxY + 8, width: frame.width, height: nameLabel.frame.height)
        populationLabel.frame = CGRect(x: 0, y: regionLabel.frame.maxY + 8, width: frame.width, height: nameLabel.frame.height)
    }
}
